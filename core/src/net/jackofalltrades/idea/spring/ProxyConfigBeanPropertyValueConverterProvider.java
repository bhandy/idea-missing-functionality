package net.jackofalltrades.idea.spring;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.Condition;
import com.intellij.openapi.util.Pair;
import com.intellij.psi.JavaPsiFacade;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiClassType;
import com.intellij.psi.PsiType;
import com.intellij.psi.search.GlobalSearchScope;
import com.intellij.psi.util.PropertyUtil;
import com.intellij.psi.util.PsiTypesUtil;
import com.intellij.psi.xml.XmlTag;
import com.intellij.spring.CommonSpringModel;
import com.intellij.spring.model.BeanService;
import com.intellij.spring.model.SpringBeanPointer;
import com.intellij.spring.model.converters.SpringBeanResolveConverter;
import com.intellij.spring.model.xml.beans.ListOrSet;
import com.intellij.spring.model.xml.beans.SpringBean;
import com.intellij.spring.model.xml.beans.SpringPropertyDefinition;
import com.intellij.spring.model.xml.beans.SpringValue;
import com.intellij.util.Function;
import com.intellij.util.containers.ContainerUtil;
import com.intellij.util.xml.ConvertContext;
import com.intellij.util.xml.Converter;
import com.intellij.util.xml.DomElement;
import com.intellij.util.xml.DomManager;
import com.intellij.util.xml.DomUtil;
import com.intellij.util.xml.GenericDomValue;
import com.intellij.util.xml.converters.values.GenericDomValueConvertersRegistry;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * Implementation of GenericDomValueConvertersRegistry.Provider which provides a converter for the values of the
 * beanNames and interceptorNames properties of the ProxyFactoryBean within Spring configuration files.
 *
 * @author bhandy
 */
public class ProxyConfigBeanPropertyValueConverterProvider implements GenericDomValueConvertersRegistry.Provider {

    static final Set<String> SUPPORTED_PROPERTIES = ImmutableSet.of(Constants.INTERCEPTOR_NAMES_PROPERTY,
            Constants.BEAN_NAMES_PROPERTY);
    static final List<String> ADVICE_CLASS_NAMES = Arrays.asList(Constants.ADVICE_INTERFACE_CLASS,
            Constants.ADVISOR_INTERFACE_CLASS, Constants.INTERCEPTOR_INTERFACE_CLASS);

    /**
     * @return The Converter instance for converting the targetName and interceptorNames values into a references to
     * their corresponding Spring bean definitions.
     */
    @Override
    public Converter getConverter() {
        return ProxyFactorySpringBeanResolveConverter.INSTANCE;
    }

    /**
     * @return The Condition for evaluate whether the reference converter should be applied.
     */
    @Override
    public Condition<Pair<PsiType, GenericDomValue>> getCondition() {
        return IsProxyConfigBeanCondition.INSTANCE;
    }

    /**
     * Implementation of Condition which evaluates a GenericDomValue to determine whether the Converter should be
     * applied.
     *
     * @author bhandy
     */
    private static class IsProxyConfigBeanCondition implements Condition<Pair<PsiType, GenericDomValue>> {

        static IsProxyConfigBeanCondition INSTANCE = new IsProxyConfigBeanCondition();

        /**
         * Evaluates the provided Pair to determine whether or not GenericDomValue's value should be converted.
         *
         * @param domValuePair The Pair of PsiType and GenericDomValue to evaluate.
         * @return true, if and only if the GenericDomValue is a child of a bean definition of type ProxyFactoryBean
         * and the GenericDomValue is a child of a SpringPropertyDefinition with a name of either beanNames or
         * interceptorNames; otherwise, false.
         */
        @Override
        public boolean value(Pair<PsiType, GenericDomValue> domValuePair) {
            JavaPsiFacade psiFacade = JavaPsiFacade.getInstance(domValuePair.getSecond().getManager().getProject());
            SpringPropertyDefinition property = DomUtil.getParentOfType(domValuePair.getSecond(),
                    SpringPropertyDefinition.class, true);

            return isProxyConfigType(domValuePair.getSecond(), psiFacade) && isSupportedProperty(property);
        }

        private boolean isSupportedProperty(SpringPropertyDefinition property) {
            return property != null && SUPPORTED_PROPERTIES.contains(property.getPropertyName());
        }

        /**
         * Evaluates the GenericDomValue to verify the class defined is a descendent of ProxyConfig.
         *
         * @param domValue  The GenericDomValue to evaluate.
         * @param psiFacade The JavaPsiFacade for the project for looking up the PsiClass instance of ProxyFactoryBean.
         * @return true, if and only if the parent Spring bean definition is of type ProxyFactoryBean; otherwise, false.
         */
        private boolean isProxyConfigType(GenericDomValue domValue, JavaPsiFacade psiFacade) {
            Module module = domValue.getModule();

            if (module != null) {
                SpringPropertyDefinition property = DomUtil.getParentOfType(domValue, SpringPropertyDefinition.class,
                        true);

                if (property != null) {
                    SpringBean bean = DomUtil.getParentOfType(domValue, SpringBean.class, true);

                    if (bean != null) {
                        PsiClass proxyConfigBeanClass = psiFacade.findClass(Constants.PROXY_CONFIG_CLASS,
                                GlobalSearchScope.moduleWithDependenciesAndLibrariesScope(module));
                        PsiClass beanClass = bean.getBeanClass();

                        return (beanClass != null && proxyConfigBeanClass != null
                                && (beanClass.isInheritor(proxyConfigBeanClass, true)));
                    }
                }
            }

            return false;
        }
    }

    /**
     * Extension of SpringBeanResolveConverter which provides code completion assistance for the values of beanNames and
     * interceptorNames property values.
     *
     * @author bhandy
     */
    private static class ProxyFactorySpringBeanResolveConverter extends SpringBeanResolveConverter {

        private static final ProxyFactorySpringBeanResolveConverter INSTANCE =
                new ProxyFactorySpringBeanResolveConverter();

        @NotNull
        @Override
        public Collection<SpringBeanPointer> getVariants(ConvertContext context) {
            CommonSpringModel model = getSpringModel(context);

            return getVariants(context, false, false, getRequiredClasses(context), model);
        }

        @NotNull
        @Override
        public List<PsiClassType> getRequiredClasses(ConvertContext context) {
            CommonSpringModel model = getSpringModel(context);
            DomElement domValue = context.getInvocationElement();
            SpringBean springBean = DomUtil.getParentOfType(domValue, SpringBean.class, true);
            SpringPropertyDefinition property = DomUtil.getParentOfType(domValue, SpringPropertyDefinition.class, true);
            List<PsiClassType> requiredClasses = Lists.newArrayList();

            collectTargetBeanEffectiveTypes(springBean, property, context, requiredClasses);
            collectAvailableAdviceAndAdvisorTypes(springBean, property, model, requiredClasses);

            return requiredClasses;
        }

        private void collectTargetBeanEffectiveTypes(SpringBean parent, SpringPropertyDefinition property,
                                                     ConvertContext context, List<PsiClassType> types) {
            if (property == null || parent == null) {
                return;
            }

            String propertyName = property.getPropertyName();
            if (Constants.INTERCEPTOR_NAMES_PROPERTY.equals(propertyName)) {

                /*
                 * In order to have the target suggestions available for interceptorNames, there can be no target
                 * property.  This includes target, targetName and targetSource.
                 */
                if (beanClassHasTargetProperty(parent) && hasNoTargetProperty(parent)
                        && isLastValueInListOrSet(context.getInvocationElement())) {
                    for (PsiClass effectiveType : BeanService.getInstance().getEffectiveBeanTypes(parent)) {
                        types.add(PsiTypesUtil.getClassType(effectiveType));
                    }
                }
            }
        }

        private void collectAvailableAdviceAndAdvisorTypes(SpringBean parent, SpringPropertyDefinition property,
                                                           CommonSpringModel model, List<PsiClassType> types) {
            if (property == null || parent == null) {
                return;
            }

            String propertyName = property.getPropertyName();
            if (SUPPORTED_PROPERTIES.contains(propertyName)) {
                Module module = model.getModule();
                Project project = (module == null) ? null : module.getProject();

                assert project != null : "The project should not be null";

                final JavaPsiFacade psiFacade = JavaPsiFacade.getInstance(project);
                final GlobalSearchScope searchScope = GlobalSearchScope.moduleWithDependenciesAndLibrariesScope(module);
                List<PsiClassType> acceptableBeanTypes = ContainerUtil.mapNotNull(ADVICE_CLASS_NAMES,
                        new Function<String, PsiClassType>() {
                            @Override
                            public PsiClassType fun(String className) {
                                PsiClass psiClass = psiFacade.findClass(className, searchScope);
                                if (psiClass == null) {
                                    return null;
                                }

                                return PsiTypesUtil.getClassType(psiClass);
                            }
                        });

                types.addAll(acceptableBeanTypes);
            }
        }

        /**
         * Checks the properties on the bean class including the inheritor chain to verify there are target
         * properties defined.
         *
         * @param bean The SpringBean to validate.
         * @return true, if and only if, the bean class has a target property available.
         */
        private boolean beanClassHasTargetProperty(SpringBean bean) {
            PsiClass beanClass = bean.getBeanClass();
            if (beanClass == null) {
                return true;
            }

            Set<String> properties = Sets.newTreeSet(Lists.newArrayList(PropertyUtil.getWritableProperties(beanClass,
                    true)));

            return properties.contains(Constants.TARGET_NAME_PROPERTY)
                    || properties.contains(Constants.TARGET_PROPERTY)
                    || properties.contains(Constants.TARGET_SOURCE_PROPERTY);
        }

        /**
         * Checks the provided Spring bean definition for a property identifying a target bean to proxy.
         *
         * @param bean The bean to query.
         * @return true, if and only if the provided Spring bean definition has a target, targetName or targetSource
         * property; otherwise, false.
         */
        private boolean hasNoTargetProperty(SpringBean bean) {
            return (bean.getProperty(Constants.TARGET_NAME_PROPERTY) == null
                    && bean.getProperty(Constants.TARGET_PROPERTY) == null
                    && bean.getProperty(Constants.TARGET_SOURCE_PROPERTY) == null);
        }

        /**
         * @param domElement The SpringValue instance descendant of a Spring bean property names interceptorNames.
         * @return true, if and only if the provided DomElement is the last "value" element for the interceptorNames
         * property.
         */
        private boolean isLastValueInListOrSet(DomElement domElement) {
            if (domElement instanceof SpringValue) {
                DomManager manager = domElement.getManager();
                ListOrSet valueCollection = DomUtil.getParentOfType(domElement, ListOrSet.class, true);
                if (valueCollection != null) {
                    XmlTag valueCollectionElement = valueCollection.getXmlTag();
                    XmlTag[] children = valueCollectionElement.getSubTags();
                    if (children.length > 0) {
                        XmlTag lastChild = children[children.length - 1];
                        return domElement == manager.getDomElement(lastChild);
                    }
                }
            }

            return false;
        }

    }

}
