package net.jackofalltrades.idea.spring;

/**
 * This class is used strictly for defining constants.  This class cannot be extended because of the private default
 * constructor.  This is by design.  See http://en.wikipedia.org/wiki/Constant_interface for more information.
 *
 *
 * @author bhandy
 */
public final class Constants {

    /** The qualified class name for the ProxyFactoryBean class within the Spring Framework. */
    public static final String PROXY_CONFIG_CLASS = "org.springframework.aop.framework.ProxyConfig";
    public static final String PROXY_FACTORY_CLASS = "org.springframework.aop.framework.ProxyFactoryBean";

    /** The qualified class name for the interfaces allowed to be set for the interceptorNames property. */
    public static final String INTERCEPTOR_INTERFACE_CLASS = "org.aopalliance.intercept.Interceptor";
    public static final String ADVICE_INTERFACE_CLASS = "org.aopalliance.aop.Advice";
    public static final String ADVISOR_INTERFACE_CLASS = "org.springframework.aop.Advisor";

    /** Properties of type String where the name of a bean is expected. */
    public static final String TARGET_NAME_PROPERTY = "targetName";
    public static final String INTERCEPTOR_NAMES_PROPERTY = "interceptorNames";
    public static final String BEAN_NAMES_PROPERTY = "beanNames";

    /** The target properties of ProxyFactoryBean. */
    public static final String TARGET_PROPERTY = "target";
    public static final String TARGET_SOURCE_PROPERTY = "targetSource";

    /**
     * This is done in favor of creating an interface for the constants.  Interfaces can be implemented, but a class
     * with a private constructor cannot be extended, and this is the point.  These constant should be referenced using
     * this class and not inherited.
     */
    private Constants() {

    }
}
