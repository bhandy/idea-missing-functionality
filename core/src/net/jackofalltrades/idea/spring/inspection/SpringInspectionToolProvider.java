package net.jackofalltrades.idea.spring.inspection;

import com.intellij.codeInspection.InspectionToolProvider;

/**
 * Informs IntelliJ of the custom inspections provided by this plugin.
 *
 * @author bhandy
 */
public class SpringInspectionToolProvider implements InspectionToolProvider {

    @Override
    public Class[] getInspectionClasses() {
        return new Class[]{ UndesirableProxyConfigurationInspection.class };
    }

}
