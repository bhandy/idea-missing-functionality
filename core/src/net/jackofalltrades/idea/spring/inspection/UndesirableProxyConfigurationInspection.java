package net.jackofalltrades.idea.spring.inspection;

import com.google.common.base.Strings;
import com.intellij.lang.annotation.HighlightSeverity;
import com.intellij.psi.PsiClass;
import com.intellij.psi.util.ClassUtil;
import com.intellij.spring.SpringBundle;
import com.intellij.spring.model.CommonSpringBean;
import com.intellij.spring.model.SpringBeanPointer;
import com.intellij.spring.model.scope.SpringBeanScope;
import com.intellij.spring.model.xml.beans.Beans;
import com.intellij.spring.model.xml.beans.SpringBean;
import com.intellij.spring.model.xml.beans.SpringPropertyDefinition;
import com.intellij.util.xml.DomElement;
import com.intellij.util.xml.GenericDomValue;
import com.intellij.util.xml.highlighting.BasicDomElementsInspection;
import com.intellij.util.xml.highlighting.DomElementAnnotationHolder;
import com.intellij.util.xml.highlighting.DomHighlightingHelper;
import net.jackofalltrades.idea.spring.Constants;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;

/**
 * Implementation of InspectionTool which provides support for identifying prototype proxy instances with singleton
 * targets to avoid threading issues in an application.
 *
 * @author bhandy
 */
public class UndesirableProxyConfigurationInspection extends BasicDomElementsInspection<Beans> {

    public UndesirableProxyConfigurationInspection() {
        super(Beans.class);
    }

    @Override
    protected void checkDomElement(DomElement element, DomElementAnnotationHolder holder, DomHighlightingHelper helper) {
        if (element instanceof SpringBean) {
            SpringBean bean = (SpringBean) element;
            if (isProxyFactoryBean(bean) && !createsSingletonProxies(bean)) {
                if (bean.getProperty(Constants.TARGET_PROPERTY) != null) {
                    holder.createProblem(element, HighlightSeverity.WARNING,
                            "Prototype proxies with singleton targets are undesirable.");
                }

                CommonSpringBean targetBean = resolveTargetName(bean);
                if (targetBean instanceof SpringBean) {
                    SpringBean target = (SpringBean) targetBean;
                    SpringBeanScope targetScope = target.getScope().getValue();
                    if (targetScope == null || targetScope.getValue().equals("singleton")) {
                        holder.createProblem(element, HighlightSeverity.WARNING,
                                "Prototype proxies with singleton targets are undesirable.");
                    }
                }
            }
        }
    }

    @Nls
    @NotNull
    @Override
    public String getGroupDisplayName() {
        return SpringBundle.message("model.inspection.display.name");
    }

    @Nls
    @NotNull
    @Override
    public String getDisplayName() {
        return "Prototype Proxy w/ Singleton Target";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "PrototypeProxySingletonTarget";
    }

    private boolean isProxyFactoryBean(SpringBean bean) {
        PsiClass proxyFactoryBeanClass = ClassUtil.findPsiClass(bean.getPsiManager(), Constants.PROXY_FACTORY_CLASS);
        PsiClass beanClass = bean.getBeanClass();

        return beanClass != null && beanClass.equals(proxyFactoryBeanClass);
    }

    private boolean createsSingletonProxies(SpringBean bean) {
        SpringPropertyDefinition singletonProperty = bean.getProperty("singleton");
        if (singletonProperty != null) {
            GenericDomValue<?> valueElement = singletonProperty.getValueElement();
            if (valueElement != null && !Strings.isNullOrEmpty(valueElement.getStringValue())) {
                return Boolean.valueOf(valueElement.getStringValue());
            }
        }

        return true;
    }

    private CommonSpringBean resolveTargetName(SpringBean bean) {
        SpringPropertyDefinition targetProperty = bean.getProperty(Constants.TARGET_NAME_PROPERTY);
        if (targetProperty != null) {
            GenericDomValue<?> targetDomValue = targetProperty.getValueElement();
            if (targetDomValue != null) {
                Object value = targetDomValue.getValue();
                if (value instanceof SpringBeanPointer) {
                    return ((SpringBeanPointer) value).getSpringBean();
                } else if (value instanceof SpringBean) {
                    return (SpringBean) value;
                }
            }
        }

        return null;
    }

}
