package net.jackofalltrades.idea.action;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.DataKeys;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.project.ProjectManager;
import com.intellij.openapi.util.Disposer;

/**
 * Action implementation which searches for other open projects in order to close them in one fell swoop.
 *
 * @author bhandy
 */
public class CloseOtherProjectsAction extends AnAction {

    @Override
    public void update(AnActionEvent e) {
        ProjectManager projectManager = ProjectManager.getInstance();
        Project project = DataKeys.PROJECT.getData(e.getDataContext());
        e.getPresentation().setEnabled(project != null && project != projectManager.getDefaultProject()
                && projectManager.getOpenProjects().length > 1);
    }

    @Override
    public void actionPerformed(AnActionEvent e) {
        Project currentProject = DataKeys.PROJECT.getData(e.getDataContext());
        ProjectManager projectManager = ProjectManager.getInstance();
        for (Project project : projectManager.getOpenProjects()) {
            if (project == currentProject) {
                continue;
            }

            final Project closeableProject = project;
            projectManager.closeProject(closeableProject);
            ApplicationManager.getApplication().runWriteAction(new Runnable() {
                @Override
                public void run() {
                    Disposer.dispose(closeableProject);
                }
            });
        }
    }
}
